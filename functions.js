function find() {
  var input, filter, rootDiv, classDiv, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  rootDiv = document.getElementById("main");
  classDiv = rootDiv.getElementsByClassName("card");
  for (i = 0; i < classDiv.length; i++) {
    
    td = classDiv[i].getElementsByTagName("p")[0]; // name
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        classDiv[i].style.display = "";
      } else {
        classDiv[i].style.display = "none";
      }
    }       
  }
}

function sortByEpisodes() {
  var rootDiv, classDiv, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  rootDiv = document.getElementById("main");
  switching = true;
  dir = "asc"; 
  while (switching) {
    //start by saying: no switching is done:
    switching = false;

    classDiv = rootDiv.getElementsByClassName("card");

    for (i = 0; i < (classDiv.length - 1); i++) {
      shouldSwitch = false;
      x = classDiv[i].getElementsByClassName("episodes")[0].getElementsByTagName("p").length;
      y = classDiv[i + 1].getElementsByClassName("episodes")[0].getElementsByTagName("p").length;
      if (dir == "asc") {
        if (x > y) {
          shouldSwitch = true;
          break;
        } else if(x == y) {
          var a = classDiv[i].getElementsByTagName("span")[0].innerText;
          var b = classDiv[i + 1].getElementsByTagName("span")[0].innerText;
          if(compareDate(a, b) == -1){
            shouldSwitch = true;
            break;
          }
        }
      } else if (dir == "desc") {
        if (x < y) {
          shouldSwitch = true;
          break;
        } else if(x == y) {
          var a = classDiv[i].getElementsByTagName("span")[0].innerText;
          var b = classDiv[i + 1].getElementsByTagName("span")[0].innerText;
          if(compareDate(a, b) == 1){
            shouldSwitch = true;
            break;
          }
        }
      }
    }
    if (shouldSwitch) {
      classDiv[i].parentNode.insertBefore(classDiv[i + 1], classDiv[i]);
      switching = true;
      switchcount ++;      
    } else {
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}

function compareDate(sDateA, sDateB)
{
  var a = new Date(sDateA).getTime();
  var b = new Date(sDateB).getTime();

  if(a < b)
  {
    return -1;
  } else if(a > b){
    return 1;
  } 
  return 0;
}

function sortByDate() {
  var rootDiv, classDiv, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  rootDiv = document.getElementById("main");
  switching = true;
  dir = "asc"; 
  while (switching) {
    //start by saying: no switching is done:
    switching = false;

    classDiv = rootDiv.getElementsByClassName("card");

    for (i = 0; i < (classDiv.length - 1); i++) {
      shouldSwitch = false;
      x = classDiv[i].getElementsByTagName("span")[0].innerText;
      y = classDiv[i + 1].getElementsByTagName("span")[0].innerText;
      if (dir == "asc") {
        if (compareDate(x, y) == 1) {
          shouldSwitch = true;
          break;
        }
      } else if (dir == "desc") {
        if (compareDate(x, y) == -1) {
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      classDiv[i].parentNode.insertBefore(classDiv[i + 1], classDiv[i]);
      switching = true;
      switchcount ++;      
    } else {
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}

