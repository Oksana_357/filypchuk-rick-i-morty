class Character {
    created;
    species;
    image;
    episode;
    name;
    location;
}

const url = 'https://rickandmortyapi.com/api/character';
let main = document.getElementById("main");
let characters = [];

fetch(url)
    .then(res => res.json())
    .then(data => {
        const rawData = data.results;
        chs = [];
        rawData.map(character => {
           const ch = new Character();
           ch.created = character.created;
           ch.species = character.species;
           ch.image = character.image;
           ch.episodes = character.episode;
           ch.name = character.name;
           ch.location = character.location;
           chs.push(ch);   
        })
        return chs;
    }).then(res => {
      show(res)
      characters = res;
    }).catch((error) =>  console.log(error));

function show(characters) {
    for (let step = 0; step < characters.length; step++) {
        main.append(createAppend(characters[step]));
    }
}


function createAppend(character) {
    const element = document.createElement("div");
    element.classList.add("card");
    element.id = character.name;
    const img = document.createElement("img");
    img.setAttribute("src",character.image);
    img.classList.add("avatar");

    const infoContainer = document.createElement("div");
    infoContainer.classList.add("container");

    const created = document.createElement("span");
    created.innerText = character.created;

    const name = createEl("p", character.name);
    const species = createEl("p", character.species);
    const episodes = createEpisodes(character.episodes);
    const location = createEl("p", character.location.name);

    infoContainer.append(created);
    infoContainer.append(name);
    infoContainer.append(species);
    infoContainer.append(location);
    infoContainer.append(createButton(character.name));

    element.append(img);  
    element.append(infoContainer);
    element.append(episodes);

    return element;
}

function createEl(tag, text) {
    const element = document.createElement(tag);
    element.innerText = text;
    return element;
}

function createEpisodes(episodes) {
    const element2 = document.createElement("div");
    element2.className = "episodes";
    episodes.forEach(element => { 
        const myArr = element.split("/");
        var ep = createEl("a", myArr[myArr.length - 1]);
        ep.setAttribute("href", element)
        element2.append(ep);
    });
    return element2;
}

function createButton(elementId) {
    const element = document.createElement("button");
    element.setAttribute("onclick", "remove('" + elementId + "')");
    element.innerText = "remove";
    return element;
}

function remove(elementId) {
    document.getElementById(elementId).remove();
  }